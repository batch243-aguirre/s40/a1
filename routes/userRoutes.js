const express=require("express");
const router = express.Router();
const auth = require("../auth")

const userController=require("../controllers/userControllers");

// Route for checking email
router.post("/checkEmail", userController.checkEmailExists);


// Route for regitration
router.post("/register", userController.checkEmailExists,userController.registerUser);

// Route for log in
router.post("/login",userController.loginUser);


// S38 Activity - Code Along
router.post("/details",auth.verify, userController.getProfile);


router.get("/profile",userController.profileDetails);


router.patch("/updateRole/:userId", auth.verify , userController.updateRole);

router.post("/enroll/:courseId",auth.verify,userController.enroll);
module.exports=router;