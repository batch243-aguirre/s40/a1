const express = require("express");
const router = express.Router();
const auth = require("../auth")

const courseControllers =  require("../controllers/courseControllers.js");

// Adding course
router.post("/",auth.verify,courseControllers.addCourse);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive);

// Speficific course 
router.get("/:courseId", courseControllers.getCourse);

// update specific course
router.put("/update/:courseId",auth.verify,courseControllers.updateCourse);


// Activity s40
router.patch("/archive/:courseId",auth.verify,courseControllers.archiveCourse);

module.exports=router;